//
//  AATKitAdManager.h
//  DFPDemo
//
//  Created by Michael on 04/05/16.
//  Copyright © 2016 AddApptr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AATKit/AATKit.h>
#import <GoogleMobileAds/DFPRequest.h>
#import <GoogleMobileAds/DFPBannerView.h>
@protocol AATKitAdManagerDelegate
- (void) adManagerHasDFPAd;
- (void) adManagerHasAddApptrAd;
- (void) adManagerFailedToLoadDFPAd;
- (void) adManagerFailedToLoadAATAd;
@end

@interface AATKitAdManager : NSObject <AATKitDelegate, GADBannerViewDelegate>
@property (nonatomic, weak) UIViewController *viewController;
@property (nonatomic, assign) id<AATKitAdManagerDelegate> adManagerDelegate;
+ (id)sharedInstanceWithViewController: (UIViewController*) vc;
- (void) updateAdViewController: (UIViewController*) vc;

- (UIView*) getDFPBannerView;
- (void) loadDPFBannerAd;

- (UIView*) getAATBannerView;
- (void) loadAATBannerAd;
@end

