//
//  AATKitAdManager.m
//  DFPDemo
//
//  Created by Michael on 04/05/16.
//  Copyright © 2016 AddApptr. All rights reserved.
//
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)


#import "AATKitAdManager.h"
static NSString *DFP_AAT_DEMO_ADUNIT = @"/57201580/Martin_Banner_Test";

@interface AATKitAdManager()
@property (nonatomic, strong) id aatBannerPlacement;
@property (nonatomic, strong) DFPBannerView *dfpBannerView;
@property (nonatomic, strong) UIView *dfpBannerContainerView;
@property (nonatomic, strong) NSDictionary *DFP_AAT_DEMO_KEYWORDS;
@end

@implementation AATKitAdManager
+ (id)sharedInstanceWithViewController: (UIViewController*) vc {
    
    static dispatch_once_t p = 0;
    __strong static AATKitAdManager *_sharedObject = nil;
    
    dispatch_once(&p, ^{
        _sharedObject = [[AATKitAdManager alloc] initWithViewConroller:vc];
    });
    _sharedObject.viewController = vc;
    return _sharedObject;
}

- (instancetype) initWithViewConroller: (UIViewController*) vc{
    self = [super init];
    if (self) {
        [AATKit initWithViewController:vc delegate:self andEnableTestModeWithID:154];
        [AATKit debug:YES];
        AATKitAdType bannerType;
        
        CGRect screenRect   = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        if (screenWidth == 414.0) {
            bannerType = AATKitBanner414x53; // wrapper for 320x53
        }
        else if (screenWidth == 375.0) {
            bannerType = AATKitBanner375x53; // wrapper for 320x53
        } else {
            bannerType = AATKitBanner320x53;
        }
        
        self.viewController = vc;
        self.dfpBannerView = [self createDFPBanner];
        [self wrapDFPBannerIntoContainerView:self.dfpBannerView];
        [self configureDFPBanner];
        self.aatBannerPlacement = [AATKit createPlacementWithName:@"aat_banner" andType:bannerType];
    }
    return self;
}

- (void) configureDFPBanner {
    self.DFP_AAT_DEMO_KEYWORDS = @{@"gender" : @"male", @"sport" : @"soccer"};
    self.dfpBannerView.adUnitID = DFP_AAT_DEMO_ADUNIT;
    self.dfpBannerView.delegate = self;
    self.dfpBannerView.rootViewController = self.viewController;
}

- (void) updateAdViewController: (UIViewController*) vc {
    [AATKit setViewController: vc];
    self.dfpBannerView.rootViewController = vc;
}


- (DFPBannerView*) createDFPBanner {
    CGRect bannerSize = CGRectMake(0, 0, 320, 50); //DFP ad is only available in 320x50
    return [[DFPBannerView alloc] initWithFrame:bannerSize];
}


- (void) wrapDFPBannerIntoContainerView: (DFPBannerView*) dfpBanner{
    if(IS_IPHONE_5){ //Google uses a height of 50
        self.dfpBannerContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    } else if (IS_IPHONE_6) {
        self.dfpBannerContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 375, 50)];
    } else if (IS_IPHONE_6P) {
        self.dfpBannerContainerView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 414, 50)];
    } else {
        NSLog(@"This demo only works on iPhones >= iPhone 5");
    }
    CGFloat x_pos_in_container = (self.dfpBannerContainerView.frame.size.width - dfpBanner.frame.size.width) / 2;
    CGRect positionInContainer = CGRectMake(x_pos_in_container,
                                            0.0,
                                            dfpBanner.frame.size.width,
                                            dfpBanner.frame.size.height);
    dfpBanner.frame = positionInContainer;
    [self.dfpBannerContainerView addSubview:dfpBanner];
    [self.dfpBannerContainerView bringSubviewToFront:dfpBanner];
}

- (UIView*) getAATBannerView {
    return [AATKit getPlacementView: self.aatBannerPlacement];
}

- (UIView*) getDFPBannerView {
    return self.dfpBannerContainerView;
}


- (void) loadAATBannerAd {
    [AATKit reloadPlacement: self.aatBannerPlacement];
}

- (void) loadDPFBannerAd {
    DFPRequest *request = [DFPRequest new];
    request.customTargeting = self.DFP_AAT_DEMO_KEYWORDS;
    [self.dfpBannerView loadRequest:request];
}


#pragma mark - DFP delegate methods
- (void)adViewDidReceiveAd:(GADBannerView *)bannerView {
    NSLog(@"%s", __FUNCTION__);
    [self.adManagerDelegate adManagerHasDFPAd];
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"%s: error: %@", __FUNCTION__, error);
    [self.adManagerDelegate adManagerFailedToLoadDFPAd];
}

- (void)adViewWillPresentScreen:(GADBannerView *)adView {
    NSLog(@"%s", __FUNCTION__);
}

- (void)adViewWillLeaveApplication:(GADBannerView *)adView {
    NSLog(@"%s", __FUNCTION__);
}

- (void)adViewDidDismissScreen:(GADBannerView *)adView {
    NSLog(@"%s", __FUNCTION__);
}


#pragma mark - AATKit delegate methods
- (void) AATKitHaveAd:(id)placement {
    NSLog(@"%s", __FUNCTION__);
    [self.adManagerDelegate adManagerHasAddApptrAd];
}

- (void) AATKitNoAds:(id)placement {
    NSLog(@"%s", __FUNCTION__);
    [self.adManagerDelegate adManagerHasAddApptrAd];
}
@end

