//
//  ViewController.h
//  DFPDemo
//
//  Created by Michael on 04/05/16.
//  Copyright © 2016 AddApptr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AATKitAdManager.h"

@interface ViewController : UIViewController<AATKitAdManagerDelegate>


@end

