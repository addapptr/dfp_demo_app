//
//  ViewController.m
//  DFPDemo
//
//  Created by Michael on 04/05/16.
//  Copyright © 2016 AddApptr. All rights reserved.
//

#import "ViewController.h"
#import "AATKitAdManager.h"
@interface ViewController ()
@property (nonatomic, weak) UIView *bannerView;
@property (nonatomic, strong) AATKitAdManager *adManager;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.adManager = [AATKitAdManager sharedInstanceWithViewController:self];
    self.adManager.adManagerDelegate = self;
}


- (IBAction)loadAdButtonTouchUpInside:(id)sender {
    //1. call to DFP
    [self.adManager loadDPFBannerAd];
}

- (void) showAATBanner {
    [self placeBannerViewToTheBottom: [self.adManager getAATBannerView]];
}

- (void) showDFPBanner {
    [self placeBannerViewToTheBottom: [self.adManager getDFPBannerView]];
}

- (void) placeBannerViewToTheBottom: (UIView*) bannerView {

    CGPoint pos = CGPointMake(0, self.view.frame.size.height - bannerView.frame.size.height);
    CGRect rect = CGRectMake(pos.x, pos.y,
                             bannerView.frame.size.width,
                             bannerView.frame.size.height);
    bannerView.frame = rect;
    bannerView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [self.view addSubview: bannerView];
    [self.view bringSubviewToFront: bannerView];
}

#pragma mark - AdManager delegate methods
- (void) adManagerHasAddApptrAd {
    [self showAATBanner];
}

- (void) adManagerHasDFPAd {
    [self showDFPBanner];
}

- (void) adManagerFailedToLoadDFPAd {
    NSLog(@"No fill by DFP");
    //2. call via AddApptr
    [self.adManager loadAATBannerAd];
}

- (void) adManagerFailedToLoadAATAd {
    NSLog(@"No fill by AddApptr SDK");
}
@end
